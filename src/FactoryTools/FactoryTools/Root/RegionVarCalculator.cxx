#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "FactoryTools/RegionVarCalculator.h"
#include "FactoryTools/strongErrorCheck.h"
#include "FactoryTools/TreeManager.h"

#include <xAODAnaHelpers/HelperFunctions.h>

#include <FactoryTools/HelperFunctions.h>

#include <algorithm>

// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator)

typedef FactoryTools::HelperFunctions HF;

