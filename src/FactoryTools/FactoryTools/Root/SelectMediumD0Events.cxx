#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <FactoryTools/SelectMediumD0Events.h>
#include <FactoryTools/HelperFunctions.h>
#include <AsgTools/MsgStream.h>
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"


// this is needed to distribute the algorithm to the workers
ClassImp(SelectMediumD0Events)

typedef FactoryTools::HelperFunctions HF;
typedef xAOD::MuonContainer MC;
typedef xAOD::MuonAuxContainer MAC;

SelectMediumD0Events :: SelectMediumD0Events (){}
EL::StatusCode SelectMediumD0Events :: setupJob (EL::Job& /*job*/){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: histInitialize (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: fileExecute (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: changeInput (bool /*firstFile*/){return EL::StatusCode::SUCCESS;}

EL::StatusCode SelectMediumD0Events :: initialize (){return EL::StatusCode::SUCCESS;}

EL::StatusCode SelectMediumD0Events :: execute (){
  xAOD::TStore* store = wk()->xaodStore();
  xAOD::TEvent* event = wk()->xaodEvent();

  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);
  auto baselineMuons = HF::createContainerInTStore<MC, MAC >("baselineMuons",store);
  auto muons_STCalib = HF::grabFromStore<MC>("STCalibMuons",store);


  std::vector<xAOD::Muon*> muons_STCalib_sorted = sortByPt(muons_STCalib);
  int counter = 0;
  for (xAOD::Muon* mu : muons_STCalib_sorted) {
    if ((int)mu->auxdata<char>("baseline") == 0) continue;
    baselineMuons.first->push_back( mu );
    counter++;
  }

  int const nMuons = counter;

  std::string regionName = "";
  if(nMuons>1){
    regionName = "MEDIUMD0";
  }

  eventInfo->auxdecor< std::string >("campaign") = m_campaign;
  eventInfo->auxdecor< bool >("doprw") = m_doprw;
  eventInfo->auxdecor< bool >("usePMGTool") = m_usePMGTool;
  eventInfo->auxdecor< float >("crossSection") = m_crossSection;
  eventInfo->auxdecor< float >("kFactor") = m_kFactor;
  eventInfo->auxdecor< float >("filterEff") = m_filterEff;
  eventInfo->auxdecor< float >("xsecUncertUp") = m_xsecUncertUp;
  eventInfo->auxdecor< float >("xsecUncertDown") = m_xsecUncertDown;
  eventInfo->auxdecor< std::string >("regionName") = regionName;
  eventInfo->auxdecor< std::string >("BSMpdgids") = m_BSMpdgids;
  ATH_MSG_INFO( "Writing to eventInfo decoration: " <<  eventInfo->auxdecor< std::string >("regionName") );
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode SelectMediumD0Events :: postExecute (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: finalize (){return EL::StatusCode::SUCCESS;}
EL::StatusCode SelectMediumD0Events :: histFinalize (){return EL::StatusCode::SUCCESS;}

std::vector<xAOD::Muon*> SelectMediumD0Events::sortByPt(MC* inputMuonContainer){
  std::vector<xAOD::Muon*> outputVector;
  for( xAOD::Muon * thisMuon : *inputMuonContainer) outputVector.push_back(thisMuon);
  std::sort(outputVector.begin(), outputVector.end(), PtOrder<xAOD::Muon>);
  return outputVector;
}
