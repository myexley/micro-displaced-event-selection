#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/IWorker.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

#include <PATInterfaces/SystematicRegistry.h>

#include "SUSYTools/SUSYObjDef_xAOD.h"

//xAOD include(s)
#include "xAODEventInfo/EventInfo.h"
#include "xAODEventInfo/EventAuxInfo.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"

//tools
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"

#include "FactoryTools/CalibrateST.h"
#include "FactoryTools/strongErrorCheck.h"
#include <FactoryTools/HelperFunctions.h>

#include <boost/algorithm/string/replace.hpp>

// this is needed to distribute the algorithm to the workers
ClassImp(CalibrateST)

typedef FactoryTools::HelperFunctions HF;


CalibrateST :: CalibrateST () :
  m_objTool("ST::SUSYObjDef_xAOD/SUSYTools") {}

EL::StatusCode CalibrateST :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}


EL::StatusCode CalibrateST :: initialize (){
  //Uncomment this if you want the whole job to fail on an unchecked status code
  // StatusCode::enableFailure();

  xAOD::TEvent* event = wk()->xaodEvent();
  //xAOD::TStore* store = wk()->xaodStore();

  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);
  isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION);
  bool const isData = !(eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) );
  bool const isAtlfast = false;

  ST::ISUSYObjDef_xAODTool::DataSource datasource = (isData ? ST::ISUSYObjDef_xAODTool::Data : (isAtlfast ? ST::ISUSYObjDef_xAODTool::AtlfastII : ST::ISUSYObjDef_xAODTool::FullSim) );

  if( doPRW == true ){
    ATH_MSG_INFO("Do PRW: true. Set to false in run script if not needed");

    if( PRWLumiCalcFileNames == notSetString() ){
      ATH_MSG_ERROR("you need to set the lumicalc file path in your run script!");
      return EL::StatusCode::FAILURE;
    }

    if( PRWConfigFileNames == notSetString() ){
      ATH_MSG_ERROR("you need to set the pileup reweighting file path in your run script!");
      return EL::StatusCode::FAILURE;
    }
  }
  else {
    ATH_MSG_INFO("Do PRW: FALSE. Set to TRUE in run script if needed.");
  }

  m_objTool.setTypeAndName("ST::SUSYObjDef_xAOD/SUSYObjDef_xAOD" + systVar.name());
  STRONG_CHECK( m_objTool.setProperty("DataSource", datasource) );
  STRONG_CHECK( m_objTool.setProperty("OutputLevel", this->msg().level()) );


  ATH_MSG_DEBUG("analysisName set. Trying to open config file " << SUSYToolsConfigFileName);
  STRONG_CHECK( m_objTool.setProperty("ConfigFile", SUSYToolsConfigFileName) );
  

  if( doPRW && isMC ){
    std::vector<std::string> PRWFiles;
    PRWFiles = HF::parseCSV(PRWConfigFileNames);
    std::vector<std::string> lumiCalcFiles = HF::parseCSV(PRWLumiCalcFileNames);

    STRONG_CHECK( m_objTool.setProperty("PRWConfigFiles", PRWFiles) );
    STRONG_CHECK( m_objTool.setProperty("PRWLumiCalcFiles", lumiCalcFiles) );
  }
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode CalibrateST :: execute ()
{
  xAOD::TEvent * event = wk()->xaodEvent();

  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

  //%%%%%%%%%%%%%%%%%//
  // Save "if is MC" //
  //%%%%%%%%%%%%%%%%%//
  eventInfo->auxdecor<float>("isMC") = ( isMC ) ? 1 : 0 ;

  //%%%%%%%%%%%%%%%%%%%%%//
  // Pileup re-weighting //
  //%%%%%%%%%%%%%%%%%%%%%//

  if( !doPRW ){
    // Set Random Run number if you aren't applying pileup reweighting - needed for calibrations
    eventInfo->auxdecor<unsigned int>("RandomRunNumber") = 300000;
    // manually handing a run number so the random run number function is not called (requires the PRW tool)
    eventInfo->auxdecor<float>("year") =  m_objTool->treatAsYear(1e6);
  }

  if( doPRW && isMC ){
    m_objTool->ApplyPRWTool();
    eventInfo->auxdecor<float>("year") =  m_objTool->treatAsYear();
  }

  bool hasMuons = event->contains<xAOD::MuonContainer>( "Muons" );

  xAOD::MuonContainer* muons_nominal(nullptr);
  xAOD::ShallowAuxContainer* muons_nominal_aux(nullptr);
  if( hasMuons ){
    STRONG_CHECK( m_objTool->GetMuons(muons_nominal, muons_nominal_aux, true) );
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode CalibrateST :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: finalize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode CalibrateST :: histFinalize () {return EL::StatusCode::SUCCESS;}
