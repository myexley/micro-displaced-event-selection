#include <FactoryTools/AnyType.h>
#include <FactoryTools/TreeManager.h>
#include <FactoryTools/CalculateRegionVars.h>
#include <FactoryTools/CalibrateST.h>
#include <FactoryTools/HelperFunctions.h>
#include <FactoryTools/RegionVarCalculator.h>
#include <FactoryTools/RegionVarCalculator_mediumd0.h>
#include <FactoryTools/SelectMediumD0Events.h>
#include <FactoryTools/WriteOutputNtuple.h>


#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class TreeManager+;
#pragma link C++ class CalculateRegionVars+;
#pragma link C++ class CalibrateST+;
#pragma link C++ class HelperFunctions+;
#pragma link C++ class RegionVarCalculator+;
#pragma link C++ class RegionVarCalculator_mediumd0+;
#pragma link C++ class SelectMediumD0Events+;
#pragma link C++ class WriteOutputNtuple+;
#pragma link C++ class JetMakerTool+;
#pragma link C++ class TracksForJets+;

#include <FactoryTools/ModularAlgorithmBase.h>
#include <FactoryTools/ProcessorBase.h>

#pragma link C++ class ModularAlgorithmBase+;
#pragma link C++ class ProcessorBase+;

#endif
