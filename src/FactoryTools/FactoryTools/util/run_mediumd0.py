#!/usr/bin/env python

import ROOT, logging, collections, commonOptions, ast

# Workaround to fix threadlock issues with GUI
ROOT.PyConfig.StartGuiThread = False

parser = commonOptions.parseCommonOptions()

#you can add additional options here if you want
parser.add_option('--campaign', help = "Choose which campaign: mc16a, mc16d or mc16e, the correct GRL, lumicalc and PRW files will be given to the algs.",choices=("mc16a", "mc16d", "mc16e"), default="mc16a")
parser.add_option('--doprw', help = "set do prw True for if you want to do pileup reweighting and aso provide the pileup profile hist",default=False)
parser.add_option('--prw_file', help = "Give me the prw distribution hist, make sure it is for the correct pileup campaign!!", default="")
parser.add_option('--usePMGTool', help = "Use the PMG for xsec etc, if false explicitly give me them!",default=0)
parser.add_option('--crossSection', default=1)
parser.add_option('--kFactor', default=1)
parser.add_option('--filterEff', default=1)
parser.add_option('--xsecUncertUp', default=1)
parser.add_option('--xsecUncertDown', default=1)
parser.add_option('--BSMpdgids',default='1000013,2000013')
(options, args) = parser.parse_args()

job = commonOptions.initializeRunScript(options, args)

######################################################################
BSMpdgids=options.BSMpdgids
campaign = options.campaign
prw_file = options.prw_file
doprw = ast.literal_eval(options.doprw)
usePMGTool = ast.literal_eval(options.usePMGTool)
crossSection = float(options.crossSection)
kFactor = float(options.kFactor)
filterEff = float(options.filterEff)
xsecUncertUp = float(options.xsecUncertUp)
xsecUncertDown = float(options.xsecUncertDown)
logging.info("creating algorithms")
print('kfactor',kFactor)
outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);

#here we add the algorithms we want to run over
import collections
algsToRun = collections.OrderedDict()

algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] )
setattr(algsToRun["basicEventSelection"], "m_useMetaData"  , True)
setattr(algsToRun["basicEventSelection"], "m_triggerSelection", ".+" )
setattr(algsToRun["basicEventSelection"], "m_applyTriggerCut" , False )
setattr(algsToRun["basicEventSelection"], "m_applyGRLCut" , False )
setattr(algsToRun["basicEventSelection"], "m_doPUreweighting" , doprw )
setattr(algsToRun["basicEventSelection"], "m_PRWFileNames" , prw_file )

if campaign == 'mc16a':
    setattr(algsToRun["basicEventSelection"], "m_lumiCalcFileNames", "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root,$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root")
    setattr(algsToRun["basicEventSelection"], "m_GRLxml" , "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml,$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml")

if campaign == 'mc16d':
    setattr(algsToRun["basicEventSelection"], "m_lumiCalcFileNames", "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root")
    setattr(algsToRun["basicEventSelection"], "m_GRLxml" , "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml")

if campaign == 'mc16e':
    setattr(algsToRun["basicEventSelection"], "m_lumiCalcFileNames", "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root")
    setattr(algsToRun["basicEventSelection"], "m_GRLxml" , "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml")
    
algsToRun["calibrateST"]               = ROOT.CalibrateST()
algsToRun["calibrateST" ].SUSYToolsConfigFileName = "$FactoryToolsWrapper_DIR/data/FactoryTools/SUSYTools_mediumd0.conf"
algsToRun["calibrateST" ].doPRW                    = algsToRun["basicEventSelection"].m_doPUreweighting;
algsToRun["calibrateST" ].PRWConfigFileNames       = algsToRun["basicEventSelection"].m_PRWFileNames
algsToRun["calibrateST" ].PRWLumiCalcFileNames     = algsToRun["basicEventSelection"].m_lumiCalcFileNames

# Main selection and sorting algorithm
algsToRun["selectMediumD0"]        = ROOT.SelectMediumD0Events()
setattr(algsToRun["selectMediumD0"], "m_campaign", campaign)
setattr(algsToRun["selectMediumD0"], "m_doprw", doprw)
setattr(algsToRun["selectMediumD0"], "m_usePMGTool", usePMGTool)
setattr(algsToRun["selectMediumD0"], "m_crossSection", crossSection)
setattr(algsToRun["selectMediumD0"], "m_kFactor", kFactor)
setattr(algsToRun["selectMediumD0"], "m_xsecUncertUp", xsecUncertUp)
setattr(algsToRun["selectMediumD0"], "m_xsecUncertDown", xsecUncertDown)
setattr(algsToRun["selectMediumD0"], "m_filterEff", filterEff)
setattr(algsToRun["selectMediumD0"], "m_BSMpdgids", BSMpdgids)
# These are the calculators that calculate various derived quantities
algsToRun["calculateRegionVars"]                 = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].m_calculator    = ROOT.RegionVarCalculator_mediumd0()

# These correspond to writing out the various trees used in the analysis
regionName = ["MEDIUMD0"]

for region in regionName:
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = region
    algsToRun["writeOutputNtuple_"+region] = tmpWriteOutputNtuple

######################################################################

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options)

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , gridUser = options.gridUser , gridTag = options.gridTag, groupRole = options.groupRole, nEvents = options.nevents, excludeSite = options.excludeSite, gridSite = options.gridSite, extraSubmitFlags=options.extraSubmitFlags )
