# Set the minimum required CMake version:
cmake_minimum_required( VERSION 3.8 )
if( ${CMAKE_VERSION} VERSION_GREATER 3.9 )
   cmake_policy( SET CMP0068 NEW )
endif()

# Allow the development of the ATLAS CMake code using this project as a test,
# by looking for AtlasCMake from an "alternate" location as a first thing.
find_package( AtlasCMake QUIET )


# Use AnalysisBase-21.2 or AthAnalysis-21.2:
set( base_project )
if( NOT "$ENV{AnalysisBase_DIR}" STREQUAL "" )
   find_package( AnalysisBase 21.2 REQUIRED )
   set( base_project AnalysisBase ${AnalysisBase_VERSION} )
elseif( NOT "$ENV{AthAnalysis_DIR}" STREQUAL "" )
   find_package( AthAnalysis 21.2 REQUIRED )
   set( base_project AthAnalysis ${AthAnalysis_VERSION} )
endif()

# Silence some MacOS X (Apple Clang) specific warnings from the base release:
if( APPLE )
   add_compile_options( -Wno-tautological-undefined-compare )
endif()

# Declare the project:
atlas_project( FactoryToolsWrapper 1.0.0
   USE ${base_project} )

include(CTest)
atlas_cpack_setup()
